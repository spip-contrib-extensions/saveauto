<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/saveauto?lang_cible=fr_tu
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'ajouter_webmestre' => 'Ajouter le webmestre aux destinataires',

	// B
	'bouton_sauvegarder' => 'Sauvegarder la base',

	// C
	'colonne_auteur' => 'Créé par',
	'colonne_nom' => 'Nom',

	// E
	'erreur_impossible_creer_verifier' => 'Impossible de créer le fichier @fichier@, vérifie les droits d’écriture du répertoire @rep_bases@.',
	'erreur_impossible_liste_tables' => 'Impossible de lister les tables de la base.',
	'erreur_probleme_donnees_corruption' => 'Probleme avec les donnees de @table@, corruption possible !',
	'erreur_repertoire_inaccessible' => 'Le répertoire @rep@ est inaccessible en écriture.',
	'erreur_repertoire_perso_inaccessible' => 'Le répertoire @rep@ configuré n’est pas accessible : utilisation du répertoire des sauvegardes de SPIP à la place',

	// H
	'help_cfg_generale' => 'Ces paramètres de configuration s’appliquent à toutes les sauvegardes, manuelles ou automatiques.',
	'help_contenu' => 'Choisis les paramètres de contenu de ton fichier de sauvegarde.',
	'help_contenu_auto' => 'Choisir le contenu des sauvegardes automatiques.',
	'help_frequence' => 'Saisir la fréquence des sauvegardes automatiques en jours.',
	'help_liste_tables' => 'Par défaut, toutes les tables SPIP sont exportées à l’exception des tables @noexport@. Si tu souhaites choisir précisément les tables à sauvegarder (ainsi que des tables non SPIP) ouvre la liste en décochant la case ci-dessous.',
	'help_mail_max_size' => 'Certaines bases de données peuvent dépasser la taille maximale des fichiers joints pour un mail. Vérifie auprès de ton fournisseur de mail pour connaître la taille maximale qu’il autorise. La limite par défaut est de 2Mo.',
	'help_max_zip' => 'Le fichier de sauvegarde est automatiquement zippé si sa taille est inférieure à un seuil. Saisir ce seuil en Mo.',
	'help_nbr_garder' => 'Indiquer le nombre minimum de sauvegardes à conserver, indépendamment du critère d’âge',
	'help_notif_active' => 'Activer l’envoi des sauvegardes par mail',
	'help_notif_mail' => 'Saisir les adresses en les séparant par des virgules ",".',
	'help_obsolete' => 'Détermine à partir de combien de jours une archive est considérée comme obsolète et automatiquement supprimée du serveur.
	 								 		Mets -1 pour désactiver cette fonctionnalité',
	'help_prefixe' => 'Optionnel : mettre un préfixe au nom du fichier de sauvegarde',
	'help_repertoire' => 'Pour utiliser un répertoire de stockage différent de celui des sauvegardes SPIP, indique son chemin depuis la racine du site (avec / à la fin)',
	'help_restauration' => '<strong>Attention !!!</strong> les sauvegardes réalisées ne sont <strong>pas au format de celles de SPIP</strong> :
   										 		Inutile d’essayer de les utiliser avec l’outil d’administration de Spip.<br /><br />
													Pour toute restauration, il faut utiliser l’interface <strong>phpMyAdmin</strong> de ton serveur de base de données : dans l’onglet <strong>"SQL"</strong> utiliser le bouton
													<strong>"Emplacement du fichier texte"</strong> pour sélectionner le fichier de sauvegarde (cocher l’option "gzippé" si nécessaire) puis valider.<br /><br />
													Les sauvegardes <strong>xxxx.gz</strong> ou <strong>xxx.sql</strong> contiennent un fichier au format SQL avec les commandes permettant d’<strong>effacer</strong> les tables existantes du SPIP et de les <strong>remplacer</strong> par les données archivées. Les données <strong>plus récentes</strong> que celles de la sauvegarde seront donc <strong>PERDUES</strong> !',
	'help_sauvegarde_1' => 'Cette option te permet de sauvegarder la structure et le contenu de la base dans un fichier au format MySQL qui sera stocké dans le répertoire tmp/dump/. Le fichier se nomme <em>@prefixe@_aaaammjj_hhmmss.</em>. Le préfixe des tables est conservé.',
	'help_sauvegarde_2' => 'La sauvegarde automatique est activée (fréquence en jours : @frequence@).',

	// I
	'info_sql_auteur' => 'Auteur : ',
	'info_sql_base' => 'Base : ',
	'info_sql_compatible_phpmyadmin' => 'Fichier SQL 100% compatible PHPMyadmin',
	'info_sql_date' => 'Date : ',
	'info_sql_debut_fichier' => 'Début du fichier',
	'info_sql_donnees_table' => 'Données de @table@',
	'info_sql_fichier_genere' => 'Ce fichier est généré par le plugin saveauto',
	'info_sql_fin_fichier' => 'Fin du fichier',
	'info_sql_ipclient' => 'IP Client : ',
	'info_sql_mysqlversion' => 'Version MySQL : ',
	'info_sql_os' => 'OS Serveur : ',
	'info_sql_phpversion' => 'Version PHP : ',
	'info_sql_plugins_utilises' => '@nb@ plugins utilisés :',
	'info_sql_serveur' => 'Serveur : ',
	'info_sql_spip_version' => 'Version de SPIP : ',
	'info_sql_structure_table' => 'Structure de la table @table@',

	// L
	'label_donnees' => 'Données des tables : ',
	'label_frequence' => 'Fréquence de la sauvegarde : tous les ',
	'label_mail_max_size' => 'Taille maximale des fichiers à attacher aux mails (en Mo) :',
	'label_max_zip' => 'Seuil des zips',
	'label_nbr_garder' => 'Combien de sauvegardes doit-on garder',
	'label_nettoyage_journalier' => 'Activer le nettoyage journalier des archives',
	'label_notif_active' => 'Activer les notifications',
	'label_notif_mail' => 'Adresses email à notifier',
	'label_obsolete_jours' => 'Sauvegardes considérées obsolètes après : ',
	'label_prefixe_sauvegardes' => 'Préfixe pour les sauvegardes : ',
	'label_repertoire_sauvegardes' => 'Répertoire',
	'label_sauvegarde_reguliere' => 'Activer la sauvegarde régulière',
	'label_structure' => 'Structure des tables : ',
	'label_tables_non_spip' => 'Tables non SPIP',
	'label_toutes_tables' => 'Sauvegarder toutes les tables de SPIP',
	'legend_cfg_generale' => 'Paramètres généraux des sauvegardes',
	'legend_cfg_notification' => 'Notifications',
	'legend_cfg_sauvegarde_reguliere' => 'Traitements automatiques',

	// M
	'message_aucune_sauvegarde' => 'Il n’y a aucune sauvegarde.',
	'message_cleaner_sujet' => 'Nettoyage des sauvegardes',
	'message_notif_cleaner_intro' => 'La suppression automatique des sauvegardes obsolètes (dont la date est antérieure à @duree@ jours) a été effectuée avec succès. Les fichiers suivants ont été supprimés : ',
	'message_notif_sauver_intro' => 'La sauvegarde de la base @base@ a été effectuée avec succès par l’auteur @auteur@.',
	'message_sauvegarde_nok' => 'Erreur lors de la sauvegarde SQL de la base.',
	'message_sauvegarde_ok' => 'La sauvegarde SQL de la base a été faite avec succès.',
	'message_sauver_sujet' => 'Sauvegarde de la base @base@',
	'message_telechargement_nok' => 'Erreur lors du téléchargement.',

	// T
	'titre_boite_historique' => 'Sauvegardes MySQL disponibles au téléchargement dans @dossier@',
	'titre_boite_sauver' => 'Plugin Saveauto : sauvegarde SQL de la base de donnée',
	'titre_page_configurer' => 'Configuration du plugin Sauvegarde automatique',
	'titre_page_saveauto' => 'Sauvegarde de base de données'
);
