<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-saveauto?lang_cible=de
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// S
	'saveauto_description' => 'Der Backupmanager "Automatisches Backup" erstellt regelmäßig vollständige Sicherungen der MySQL-Datenbank von SPIP als.zip oder .sql Datei.

Die Sicherungen werden in einem Verzeichnis auf dem Server gespeichert (Grundeinstellung: /tmp/dump) und können per Mail verschickt werden. Die Sicherungsdateien können ab einem einstellbaren Alter automatisch gelöscht werden.

Es steht ein Interface zum manuellen Start des Backups und für die Vewaltung der Sicherungsdateien zur Verfügung.',
	'saveauto_nom' => 'Automatisches Backup',
	'saveauto_slogan' => 'Automatisches Backup der MySQL-SPIP-Datenbank'
);
